# Page Layout with Resizable Sidebar

Develop a page layout that includes a header, footer, content area, and left sidebar. Use of jQuery and a CSS preprocessor is permitted.

* Left Sidebar Requirements
* fits to the height of the content area
* the sidebar should be a static width of 250px
* resizes vertically on window resize
* if the content area is larger than the viewport, as you scroll down the page the sidebar should affix to the side and top of the viewport and fill the full height of the available view port. when scrolled to the top or the bottom of the page, the sidebar should not overlap the header or the footer. (see Figure 1)
* the json data provided should be loaded into the sidebar as a list
* clicking on an item in the list should result in showing the associated details in the content area
* when the user clicks the ‘x’ the sidebar should collapse. The user should be able to reopen it using by clicking on the ‘?’ icon. (see Figure 2)

Bonus

* implement a simple search on the sidebar list that will filter the results and only show items that match the entered keyword
* use of CSS3 features where applicable
* use of jQuery functions where applicable
* use of the revealing module pattern when writing JS
