var App = App || {}; // create a namespace

App.SideNav = { // im putting this project all in one class
    init: function(container) { // i like to make my primary var a container elm incase there are multiple instaces
        // cache references to elements in DOM // the puppet strings
        this.$container = container ? $(container) : $('body');
        this.$nav = this.$container.find('.side-nav'),
        this.$navContent = this.$container.find('.dynamic-nav'),
        this.$content = this.$container.find('.custom-data'),
        this.$contentWrapper = this.$container.find('.core-content'),
        this.SideNavData = Fifty.sidebarData; // get data

        // JS rendered elements // present the actors
        this.message = "Please search and make a selection from the left navagation.";
        this.iconOpen = $('<a/>',{
            text: "?",
            href: "#",
            "class":"icon-open"
        });

        // quick template
        this.navHeader = "<div class=\"nav-header\">";
        this.navHeader += "<a class=\"help-and-support\" href=\"#\">Help and Support</a>";
        this.navHeader += "<form>";
        this.navHeader += "<input class=\"search-nav\" type=\"text\" />";
        this.navHeader += "</form>";
        this.navHeader += "<a href=\"#\" class=\"icon-close\">x</a>";
        this.navHeader += "</div>";

        // set the stage
        this.$nav.prepend( this.iconOpen )
                .prepend( this.navHeader );
        this.$content.text( this.message );
        this.updateNav();

        // start the show
        this.bindEvents();
        this.fixNav();
    },
    bindEvents: function() {
        var self = this;

        this.$nav.on('click', '.data-url', function(event) {
            event.preventDefault();
            var $this = $(this),
                id = $this.data('id');
            self.updateContent( id );
        });

        this.$nav.on('click', '.icon-open', function(event) {
            event.preventDefault();
            self.$container.addClass('open-nav');
        });

        this.$nav.on('click', '.icon-close', function(event) {
            event.preventDefault();
            // should be an external function
            self.$container.removeClass('open-nav');
            self.$nav.find('.search-nav').val('');
            self.updateNav();
        });

        this.$nav.on('keyup', '.search-nav', function(event) {
            event.preventDefault();
            var $this = $(this),
                val = $this.val();
            self.updateNav( val );
        });
    },
    buildNav: function(search) {
        // build template with data
        var template = ""; // blank template

        for (var i = this.SideNavData.length - 1; i >= 0; i--) {
            if (search) {
                if (this.SideNavData[i].name.indexOf( search ) > -1) {
                   template += "<li><a class=\"data-url\" href=\"#\" data-id=\""+this.SideNavData[i].id+"\">" + this.SideNavData[i].name + "</a></li>";
                }
            // this would show all options before search is initiated
            // } else {
            //     template += "<li><a href=\"#\" data-id=\""+this.SideNavData[i].id+"\">" + this.SideNavData[i].name + "</a></li>";
            }
        };

        if (search) { // prevent <ul> from rendering in the nav before there are list items
            template = "<ul>" + template + "</ul>";
        }

        return template;
    },
    updateNav: function(search) {
        this.$navContent.html( this.buildNav(search) );
    },
    buildContent: function(id) {
        // build template with data
        var template = "", // blank template
            data; // data namespace

        for (var i = this.SideNavData.length - 1; i >= 0; i--) {
            if (this.SideNavData[i].id === id) {
                data = this.SideNavData[i];
                break;
            }
        };

        template += "<h1>" + data.name + "</h1>";
        template += "<ul>";
        template += "<li>ID: " + data.id + "</li>";
        template += "<li>Active: " + data.isActive + "</li>";
        template += "<li>Balance: " + data.balance + "</li>";
        template += "<img src=\""+data.picture+"\" alt=\""+data.name+"\" />";
        template += "<li>Age: " + data.age + "</li>";
        template += "<li>Eye Color: " + data.eyeColor + "</li>";
        template += "<li>Gender" + data.gender + "</li>";
        template += "<li>" + data.company + "</li>";
        template += "<li>" + data.email + "</li>";
        template += "<li>" + data.phone + "</li>";
        template += "<li>" + data.address + "</li>";
        template += "<li>About: " + data.about + "</li>";
        template += "</ul>";

        return template;
    },
    updateContent: function(id) {
        this.$content.html( this.buildContent( id ) );
    },
    fixNav: function() {
        var self = this;
        $(window).scroll(function(e) {
            var scrollPosition = $(window).scrollTop(),
                nav = self.$nav.height(),
                content = (self.$contentWrapper.height() - 150),
                navPushDist = 0;
            if (scrollPosition >= 101 && nav <= content) {
                navPushDist = scrollPosition - 100;
            }
            self.$nav.css('margin-top',navPushDist+"px");
        });
    }
};



$( document ).ready(function() {
    App.SideNav.init();
});